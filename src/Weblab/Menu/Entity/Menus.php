<?php

namespace Weblab\Menu\Entity;

class Menus extends \Base\Entity\Menus
{

    protected $collection = '\Weblab\Menu\Record\Menu';

    public function getMenuBySlug($slug)
    {
        return $this->findAll()->getTree('getMenuId')->filter('getSlug', $slug)->first();
    }

}