<?php namespace Weblab\Menu\Controller;

use Weblab\Generic\Entity\Routes;

class Menu
{

    public function getAllAction(Routes $routes)
    {
        return view('all', [
            'routes' => $routes->joinTranslations()->all(),
        ]);
    }

}