<?php

namespace Weblab\Menu\Record;

use Weblab\Menu\Entity\Menus;

class Menu extends \Base\Record\Menu
{
    public $_entity = '\Weblab\Menu\Entity\Menus';

    protected static $_allMenus = [];
    protected static $_submenus = [];

    protected static function fillAllMenus($refill = false)
    {
        if ($refill || empty(self::$_allMenus)) {
            self::$_allMenus = Menus::inst()->findAll();
        }
    }

    protected static function fillSubmenus($refill = false)
    {
        if ($refill || empty(self::$_submenus)) {
            foreach (self::$_allMenus AS $menu) {
                if (!isset(self::$_submenus[$menu->getId()])) {
                    self::$_submenus[$menu->getId()] = [];
                }

                if (!isset(self::$_submenus[$menu->getMenuId()])) {
                    self::$_submenus[$menu->getMenuId()] = [];
                }

                self::$_submenus[$menu->getMenuId()][] = $menu;
            }
        }
    }

    public function getSubmenus()
    {
        self::fillAllMenus();
        self::fillSubmenus();

        return self::$_submenus[$this->getId()];
    }

    public function getTitle()
    {
        return parent::getTitle() ?: $this->getSlug();
    }
}